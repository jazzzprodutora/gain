<?php
    // Template Name: Contato
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <div class="page-contato">

        <!-- CHAMA O CABECALHO - HEADER -->
        <?php require 'templates/cabecalho.php' ?>

        
        <!-- CONTATO -->
        <section class="contato">
            <div class="container">
                <div class="itens">
                    <div class="item">
                        <h1 class="titulo">Contato</h1>

                        <!-- LOOP -->
                        <?php if(have_rows('info-contato')): while(have_rows('info-contato')) : the_row(); ?>
                            <div class="info">
                                <div class="nome"><?php the_sub_field('nome'); ?></div>
                                <div class="whatsapp">Whatsapp: <a href="https://api.whatsapp.com/send?phone=<?php the_sub_field('whatsapp'); ?>" target="_blank"><?php the_sub_field('whatsapp'); ?></a></div>
                                <div class="telefone">Tel.: <a href="tel:<?php the_sub_field('telefone'); ?>"><?php the_sub_field('telefone'); ?></a></div>
                                <div class="email">Email: <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a></div>
                            </div>
                        <?php endwhile; else : endif; ?>
                        <!-- ... -->
                        
                        <div class="linha">
                            <div class="telefone">
                                <!-- <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/telefone.png"> -->
                                <div class="info">
                                    <p class="nome">Geral</p>
                                </div>
                                <a href="mailto:atendimento@gainagenciamento.com.br">atendimento@gainagenciamento.com.br</a>
                            </div>
                            <!-- <div class="whatsapp">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp.png">
                                <a href="https://api.whatsapp.com/send?phone=<?php the_sub_field('whatsapp-destaque'); ?>" target="_blank"><?php the_field('whatsapp-destaque'); ?></a>
                            </div> -->
                        </div>
                        <div class="localizacao">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/local.png">
                            <span>Rio de Janeiro/RJ - Brasil</span>
                        </div>
                        <div class="avaliacao-online">
                            <h2>Faça sua avaliação online</h2>
                            <!-- <div class="whatsapp">Cel. e Whatsapp:<a href="#"> + 55 21 9.9974-4919</a></div>
                            <div class="telefone">Tel.:<a href="tel:+55 21 24872310"> + 55 21 2487-2310</a></div> -->
                            <div class="email">Email: <a href="mailto:guilhermeabreu@gainagenciamento.com.br">guilhermeabreu@gainagenciamento.com.br</a></div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="divisor"></div>
                    </div>

                    <div class="item">
                        <div class="formulario">
                            <form action="">
                                <input type="text" placeholder="Seu Nome">
                                <input type="email" placeholder="E-mail">
                                <input type="tel" placeholder="Telefone">
                                <textarea name="" id="" cols="30" rows="10" placeholder="Mensagem"></textarea>
                                <div class="area-botao">
                                    <button class="botao botao-principal" type="submit">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <!-- CHAMA O RODAPE -->
        <?php require 'footer.php' ?>

    </div>
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>