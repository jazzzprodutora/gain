
    <!-- RODAPE -->
    <footer class="rodape">
        <div class="container">
            <div class="localizacao">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/local.png">
                <span>Rio de Janeiro/RJ - Brasil</span>
            </div>
            <div class="instagram">
                <div class="titulo">No<br>vi<br>da<br>des</div>
                <?php echo do_shortcode( '[insta-gallery id="0"]' ); ?>
            </div>
        </div>
    </footer>
    
    

    
    <!-- CHAMA O JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/whatsapp-cabecalho.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/mudar-palavra.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/oficinas-palestras.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/menu.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>

    <!-- CHAMA O JS DO SCROLL -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <!-- JS SCROLL SUAVE -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scroll-suave.js"></script>

    <!-- LIGHTBOX JS -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery-1.7.2.min.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/lightbox.js"></script>

    <!-- SLICK -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.min.js"></script>


    <!-- CAROUSELS -->
    <script>
        $('.carousel').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
                }
            ]
        });
    </script>
    
    <script>
        $('.carousel-agenciamento-home').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
                }
            ]
        });
    </script>

    <script>
        $('.carousel-agenciados').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: false,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                }
                }
            ]
        });
    </script>

    <!-- CHAMA E INICIA O WOW.JS -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>

    <!-- LIGHTBOX JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>
    <script>
        $(document).on("click", '[data-toggle="lightbox"]', function (event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
</body>
</html>