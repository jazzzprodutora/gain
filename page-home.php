<?php
    // Template Name: Home
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <!-- CHAMA O CABECALHO - HEADER -->
    <?php require 'templates/cabecalho.php' ?>

    <section class="principal">
        <div class="container">
            <!-- PREVI SOBRE -->
            <div class="coluna-esquerda">
                <h1 class="titulo">Ética e Profissionalismo</h1>
                <div class="texto"><?php the_field('texto-sobre-home'); ?></div>
                <div class="area-botao">
                    <a href="quem-somos"><button>Saiba mais</button></a>
                </div>
            </div>

            <!-- AGENCIAMENTO -->
            <div class="coluna-direita">
                <h1 class="titulo">Agenciamento de <span class="palavra"></span></h1>
                    
                <div class="carousel-agenciamento-home itens">
                    <!-- ... -->
                    <?php
                        $args = array (
                            'post_type' => ['artistas_agenciados', 'chefs_agenciados'], //Pega os post types no array para ser mostrado nos post
                            'tipo_artista' => 'destaque',
                            'orderby' => 'rand'
                        );
                        $the_query = new WP_Query ( $args );
                    ?>
                    <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                    <div class="item">
                        <a href="<?php the_permalink();?>">
                            <div class="imagem-item">
                                <?php the_post_thumbnail()?>    
                            </div>
                            <div class="info-item">
                                <div class="nome"><?php the_title()?></div>
                                <div class="funcao"><?php the_field('funcao'); ?></div>
                            </div>
                        </a>
                    </div>

                    <?php endwhile; else: endif; ?>
                    <!-- ... -->

                </div>
                <div class="area-botao">
                    <a href="agenciados"><button class="botao botao-principal">Ver todos</button></a>
                </div>
            </div>
        </div>
    </section>


    <!-- CHAMA O RODAPE -->
    <?php require 'footer.php' ?>

    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>