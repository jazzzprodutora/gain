<?php
    // Template Name: Quem Somos
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <div class="page-quem-somos">

        <!-- CHAMA O CABECALHO - HEADER -->
        <?php require 'templates/cabecalho.php' ?>

        
        <section class="principal">
            <div class="container">
                <!-- PREVI SOBRE -->
                <div class="coluna-esquerda">
                    <div class="socios">
                        <h1 class="titulo">Sócios</h1>
                        <div class="itens">
                            <!-- LOOP -->
                            <?php if(have_rows('socios')): while(have_rows('socios')) : the_row(); ?>
                                <div class="item">
                                    <span class="nome-socio"><?php the_sub_field('nome-socio'); ?></span>
                                    <div class="linha">
                                        <div class="imagem-socio">
                                            <img src="<?php the_sub_field('imagem-socio'); ?>" alt="<?php the_sub_field('nome-socio'); ?>">
                                        </div>
                                        <div class="texto-socio">
                                            <p><?php the_sub_field('texto-socio'); ?></p>
                                        </div>
                                    </div>
                                    <!-- REDES SOCIAIS -->
                                    <div class="redes-sociais">
                                        <!-- LOOP -->
                                        <?php if(have_rows('redes-sociais-socio')): while(have_rows('redes-sociais-socio')) : the_row(); ?>
                                            <div>
                                                <a href="<?php the_sub_field('link-rede-social'); ?>" target="_blank"><img src="<?php the_sub_field('icone-rede-social'); ?>"></a>
                                            </div>
                                        <?php endwhile; else : endif; ?>
                                        <!-- ... -->
                                    </div>
                                </div>
                            <?php endwhile; else : endif; ?>
                            <!-- ... -->
                        </div>
                    </div>
                    <div class="consultoria-artistica">
                        <h1 class="titulo">Consultoria Artística</h1>
                        <a href="mailto:<?php the_field('email-consultoria-artistica'); ?>" id="email"><?php the_field('email-consultoria-artistica'); ?></a>
                    </div>
                    <div class="diferencial">
                        <h2 class="titulo">Diferencial</h2>
                        <p class="texto"><?php the_field('diferencial'); ?></p>
                    </div>
                </div>

                <!-- AGENCIAMENTO -->
                <div class="coluna-direita">
                    <h1 class="titulo">Áreas de atuação</h1>
                        
                    <div class="areas-atuacao">
                        <p><?php the_field('areas-de-atuacao'); ?></p>
                    </div>

                    <div class="historia">
                        <h1 class="titulo">História</h1>
                        <div class="texto"><?php the_field('texto-sobre'); ?></div>
                    </div>
                </div>
            </div>
        </section>


        <!-- CHAMA O RODAPE -->
        <?php require 'footer.php' ?>

    </div>
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>