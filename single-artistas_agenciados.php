<?php
    // Template Name: Single Agenciados
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <div class="single-agenciados">

        <!-- CHAMA O CABECALHO - HEADER -->
        <?php require 'templates/cabecalho.php' ?>

        
        <section class="conteudo">
            <div class="container">
                <div class="nome-agenciado"><?php the_title()?></div>

                <div class="linha">
                    <div class="coluna-esquerda">
                        <div class="imagem-agenciado">
                            <?php the_post_thumbnail()?>
                        </div>
                        <div class="itens">
                            <div class="item">
                                <h2>Nome artístico: <span><?php the_title()?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Local Nasc.: <span><?php the_field('local-nascimento'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Data de Nasc.: <span><?php the_field('data-nascimento'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Idiomas: <span><?php the_field('idiomas'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Altura: <span><?php the_field('altura'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Peso: <span><?php the_field('peso'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Sapatos: <span><?php the_field('sapatos'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Olhos: <span><?php the_field('cor-olhos'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Cabelos: <span><?php the_field('cor-cabelos'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Manequim: <span><?php the_field('manequim'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Camisa: <span><?php the_field('camisa'); ?></span></h2>
                            </div>
                            <div class="item">
                                <h2>Redes Sociais: 
                                    <div class="redes-sociais">
                                        <!-- LOOP -->
                                        <?php if(have_rows('redes-sociais-agenciado')): while(have_rows('redes-sociais-agenciado')) : the_row(); ?>
                                            <div>
                                                <a href="<?php the_sub_field('link-rede-social'); ?>"><img src="<?php the_sub_field('icone-rede-social'); ?>"></a>
                                            </div>
                                        <?php endwhile; else : endif; ?>
                                        <!-- ... -->
                                    </div>
                                </h2>
                            </div>
                        </div>
                        <!-- HABILIDADES -->
                        <div class="habilidades">
                            <a href="#" class="botao botao-principal" data-toggle="modal" data-target="#modalHabilidades">Habilidades</a>
                            <!-- MODAL AREA -->
                            <div class="modal fade" id="modalHabilidades" tabindex="-1" aria-labelledby="modalHabilidadesLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modalHabilidadesLabel"><span><?php the_title()?></span></h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="itens">
                                                <!-- LOOP -->
                                                <?php if(have_rows('habilidades-agenciado')): while(have_rows('habilidades-agenciado')) : the_row(); ?>
                                                    <div class="item">
                                                        <h4 class="tipo-habilidade"><?php the_sub_field('tipo-habilidade'); ?></h4>
                                                        <p class="nome-habilidade"><?php the_sub_field('nome-habilidade'); ?></p>
                                                    </div>
                                                <?php endwhile; else : endif; ?>
                                                <!-- ... -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="coluna-central">

                        <!-- FOTOS -->
                        <h3 class="titulo-coluna">Fotos</h3>
                        <div class="fotos">
                            <!-- LOOP -->
                            <?php if(have_rows('fotos-agenciados')): while(have_rows('fotos-agenciados')) : the_row(); ?>
                                <div class="foto">
                                    <a href="<?php the_sub_field('foto'); ?>" data-toggle="lightbox" data-gallery="gallery">
                                        <img src="<?php the_sub_field('foto'); ?>">
                                    </a>
                                </div>
                            <?php endwhile; else : endif; ?>
                            <!-- ... -->
                        </div>

                        <!-- VIDEOS -->
                        <h3 class="titulo-coluna">Vídeos</h3>
                        <div class="videos">
                            <!-- LOOP -->
                            <?php if(have_rows('videos-agenciados')): while(have_rows('videos-agenciados')) : the_row(); ?>
                                <div class="video">
                                    <a href="<?php the_sub_field('video'); ?>" data-toggle="lightbox" data-gallery="gallery">
                                        <img src="<?php the_sub_field('video'); ?>">
                                    </a>
                                </div>
                                <!-- <div class="outros-videos">
                                    <?php the_sub_field('outros-videos'); ?>
                                </div> -->
                            <?php endwhile; else : endif; ?>
                            <!-- ... -->
                        </div>

                        <!-- OUTROS VIDEOS -->
                        <div class="outros-videos">
                            <!-- LOOP -->
                            <?php if(have_rows('outros-videos-agenciados')): while(have_rows('outros-videos-agenciados')) : the_row(); ?>
                                <!-- LINK DO VIDEO -->
                                <a href="<?php the_sub_field('link-video'); ?>" data-remote="<?php the_sub_field('video'); ?>" data-toggle="lightbox" data-gallery="gallery">
                                    <!-- THUMBNAIL -->
                                    <img src="<?php the_sub_field('thumbnail-video'); ?>">
                                </a>
                            <?php endwhile; else : endif; ?>
                            <!-- ... -->
                        </div>
                    </div>

                    <div class="coluna-direita">
                        <h3 class="titulo-coluna">Currículo</h3>
                        <div class="texto"><?php the_field('curriculo'); ?></div>
                    </div>
                </div>
            </div>
        </section>


        <!-- CHAMA O RODAPE -->
        <?php require 'footer.php' ?>

    </div>
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>