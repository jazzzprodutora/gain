<?php
    // Template Name: Single Oficinas e Palestras
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <div class="single-oficinas-palestras">

        <!-- CHAMA O CABECALHO - HEADER -->
        <?php require 'templates/cabecalho.php' ?>

        <!-- CONTEUDO -->
        <section class="conteudo">
            <div class="container">
                <h1 class="titulo"><?php the_title()?></h1>

                <!-- LINHA -->
                <div class="linha">
                    <div class="coluna-esquerda">
                        <div class="imagem-destaque">
                            <?php the_post_thumbnail()?>
                        </div>
                        <div class="info">
                            <div class="item">
                                <div class="titulo-item">
                                    <h2>Público Alvo</h2>
                                </div>
                                <div class="texto-item">
                                    <p><?php the_field('publico-alvo'); ?></p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="titulo-item">
                                    <h2>Número máximo de participantes</h2>
                                </div>
                                <div class="texto-item">
                                    <p><?php the_field('capacidade'); ?></p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="titulo-item">
                                    <h2>Duração</h2>
                                </div>
                                <div class="texto-item">
                                    <p><?php the_field('duracao'); ?></p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="titulo-item">
                                    <h2>Equipamento / apoio para as aulas</h2>
                                </div>
                                <div class="texto-item">
                                    <p><?php the_field('estrutura'); ?></p>
                                </div>
                            </div>
                            <div class="item">
                                <div class="titulo-item">
                                    <h2>Investimento para o produtor do evento</h2>
                                </div>
                                <div class="texto-item">
                                    <p><?php the_field('investimento'); ?></p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="coluna-direita">
                        <div class="texto"><?php the_field('conteudo'); ?></div>
                    </div>
                </div>
            </div>
        </section>
        
        


        <!-- CHAMA O RODAPE -->
        <?php require 'footer.php' ?>

    </div>
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>