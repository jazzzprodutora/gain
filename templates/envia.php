<!-- ENVIA FORM DE CONTATO -->
<?php
    $nome = filter_input(INPUT_POST,"nome",FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST,"email",FILTER_SANITIZE_EMAIL);
    $fone = filter_input(INPUT_POST,"fone",FILTER_SANITIZE_STRING);
    $mensagem = filter_input(INPUT_POST,"mensagem",FILTER_SANITIZE_STRING);
    
    // E-mail, nome e fone precisam ser válidos.
    if($nome && $email && $fone){

        // Monta dados do de envio
        $to = "contato@tavaresesales.com.br";
        $subject =  "Contato através do site Tavares e Sales";

        // Montagem do Header
        $headers =  "MIME-Version: 1.1\n";
        $headers .= "From: contato@tavaresesales.com.br\n";
        // $headers .= "Cc: contato@tavaresesales.com.br\n";
        // $headers .= "Reply-To :{$email}\n";
        // $headers .= "X=Mailer: PHP/".phpversion()."\n";
        $headers .= "Content-type: text/html; charset=utf-8\n";

        // Monta o corpo do email
        $body = "
              <h1>{$subject}</h1>
              <table>
                <tbody>
                    <tr>
                        <td><b>Nome</b></td>
                        <td>{$nome}</td>
                    </tr>
                    <tr>
                        <td><b>E-mail</b></td>
                        <td>{$email}</td>
                    </tr>
                    <tr>
                        <td><b>Telefone</b></td>
                        <td>{$fone}</td>
                    </tr>
                    <tr>
                        <td><b>Mensagem</b></td>
                        <td>{$mensagem}</td>
                    </tr>
                </tbody>
              </table>
        ";
       
        if(mail($to,$subject,$body,$headers,"-fcontato@tavaresesales.com.br")){
             echo "<script>window.alert('Email enviado com sucesso!')</script>";
         }else{
             echo "<script>window.alert('O Email não pode ser enviado, tente novamente.')</script>";
        }
    }
?>