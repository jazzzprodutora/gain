<!-- HEADER -->
<section class="header">
    <div class="container">
        <!-- LOGO -->
        <div class="logo">
            <a href="home" id="logo-desk"><img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/logo.svg" alt="Gain - Agenciamento Artístico"></a>
            <a href="home" id="logo-mobile"><img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/logo-mobile.svg" alt="Gain - Agenciamento Artístico"></a>
            <div class="navegacao">
                <a href="quem-somos" class="link">Quem Somos</a>
                <a href="agenciados" class="link">Agenciados</a>
                <a href="oficinas-e-palestras" class="link">Oficinas e Palestras</a>
                <a href="marcas-parceiras" class="link">Marcas Parceiras</a>
                <a href="contato" class="link">Contato</a>
            </div>
            <div class="navegacao-mobile">
                <a href="quem-somos" class="link">Quem Somos</a>
                <a href="agenciados" class="link">Agenciados</a>
                <a href="oficinas-e-palestras" class="link">Oficinas e Palestras</a>
                <a href="marcas-parceiras" class="link">Marcas Parceiras</a>
                <a href="contato" class="link">Contato</a>
            </div>
        </div>
        <div class="barra-navegacao">
            <div class="menu">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/menu.svg" id="hamburguer">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/close.svg" id="close">
            </div>
            <div class="redes-sociais">

                <div class="teste-whatsapp">
                    <input id="menu-hamburguer" type="checkbox" />

                    <label for="menu-hamburguer">
                        <div class="menu-icon">
                            <span class="hamburguer">
                                <div class="item">
                                    <a href="https://api.whatsapp.com/send?phone=+5521999744919" target="_blank">Guilherme Abreu</a>
                                </div>
                                <div class="item">
                                    <a href="https://api.whatsapp.com/send?phone=+5521982811414" target="_blank">Ivan Neves</a>
                                </div>
                            </span>
                        </div>
                    </label>
                </div>


                <a href="https://www.instagram.com/gainagenciamento" target="_blank" id="instagram"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram.png"></a>
                <a href="contato" id="email"><img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/email.png"></a>
            </div>
        </div>
    </div>
</section>