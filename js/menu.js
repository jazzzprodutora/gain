var menu = document.querySelector('.menu');
var navegacao = document.querySelector('.navegacao');
var navegacaoMobile = document.querySelector('.navegacao-mobile');
var close = document.querySelector('#close');
var instagram = document.querySelector('.insta-gallery-list');
menu.addEventListener('click', () => {
  navegacao.classList.toggle('nav-ativo');
});
menu.addEventListener('click', () => {
  navegacaoMobile.classList.toggle('nav-ativo-mobile');
});
menu.addEventListener('click', () => {
  close.classList.toggle('nav-ativo-mobile');
});

var carouselAgenciados = document.querySelector('.carousel-agenciados');
var carouselAgenciadosHome = document.querySelector(
  '.carousel-agenciamento-home',
);
menu.addEventListener('click', () => {
  carouselAgenciados.classList.toggle('sobrepoe');
});
menu.addEventListener('click', () => {
  carouselAgenciadosHome.classList.toggle('sobrepoe');
});
menu.addEventListener('click', () => {
  instagram.classList.toggle('some');
});

var carouselOficinasPalestras = document.querySelector(
  '#carouselOficinasPalestras',
);
menu.addEventListener('click', () => {
  carouselOficinasPalestras.classList.toggle('sobrepoe');
});

var menuIcon = document.querySelector('.menu-icon');
menuIcon.addEventListener('click', () => {
  carouselAgenciadosHome.classList.toggle('cima');
});
