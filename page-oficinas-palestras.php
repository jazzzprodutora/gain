<?php
    // Template Name: Oficinas e Palestras
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <div class="page-oficinas-palestras">

        <!-- CHAMA O CABECALHO - HEADER -->
        <?php require 'templates/cabecalho.php' ?>

        
        <section class="oficinas-palestras">
            <div class="container">
                <h1 class="titulo">Oficinas e Palestras</h1>

                <div id="carouselOficinasPalestras" class="carousel slide" data-ride="carousel" data-interval="false">
                    <div class="carousel-inner" role="listbox">
                        <div class="carousel-item">
                            <div class="itens">

                                <!-- ... -->
                                <?php
                                    $args = array (
                                        'post_type' => 'evento', //Pega os post types no array para ser mostrado nos post
                                        'postagem' => '',
                                    );
                                    $the_query = new WP_Query ( $args );
                                ?>
                                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="item">
                                    <a href="<?php the_permalink();?>">
                                        <div class="imagem-item">
                                            <?php the_post_thumbnail()?>
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item"><?php the_title()?></div>
                                            <div class="participacao">com <?php the_field('participacao'); ?></div>

                                            <div class="publico-alvo">
                                                <p><?php the_field('publico-alvo'); ?></p>
                                            </div>
                                            <div class="capacidade">
                                                <p><?php the_field('capacidade'); ?></p>
                                            </div>
                                            <div class="duracao">
                                                <p><?php the_field('duracao'); ?></p>
                                            </div>
                                            <div class="estrutura">
                                                <p><?php the_field('estrutura'); ?></p>
                                            </div>
                                            <div class="investimento">
                                                <p><?php the_field('investimento'); ?></p>
                                            </div>
                                            <div class="conteudo">
                                                <?php the_field('conteudo'); ?>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                                <?php endwhile; else: endif; ?>
                                <!-- ... -->

                            </div>
                        </div>

                        <!-- <div class="carousel-item">
                            <div class="itens">
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                                <div class="item">
                                    <a href="">
                                        <div class="imagem-item">
                                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/4.jpg" alt="">
                                        </div>
                                        <div class="info-item">
                                            <div class="titulo-item">Oficina E-motion</div>
                                            <div class="participacao">com Raffaele Casuccio</div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <a class="carousel-control-prev" href="#carouselOficinasPalestras" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Anterior</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselOficinasPalestras" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Proximo</span>
                    </a>
                </div>
            </div>
        </section>










        <!-- RODAPE -->
        <footer class="rodape">
            <div class="container">
                <div class="localizacao">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/local.png">
                    <span>Rio de Janeiro/RJ - Brasil</span>
                </div>
                <div class="instagram">
                    <div class="titulo">No<br>vi<br>da<br>des</div>
                    <?php echo do_shortcode( '[insta-gallery id="0"]' ); ?>
                </div>
            </div>
        </footer>
    
    </div>

    <!-- CHAMA O JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/oficinas-palestras.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/menu.js"></script>
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/main.js"></script>

    <!-- CHAMA O JS DO SCROLL -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <!-- JS SCROLL SUAVE -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scroll-suave.js"></script>


    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>