<?php
    // Template Name: Marcas Parceiras
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <div class="page-marcas-parceiras">

        <!-- CHAMA O CABECALHO - HEADER -->
        <?php require 'templates/cabecalho.php' ?>

        
        <section class="marcas">
            <div class="container">
                <h1 class="titulo">Marcas Parceiras</h1>
                <div class="itens">
                    
                </div>



                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-parceiros-tab" data-toggle="tab" href="#nav-parceiros" role="tab" aria-controls="nav-parceiros" aria-selected="true">parceiros</a>
                        <a class="nav-item nav-link" id="nav-clientes-tab" data-toggle="tab" href="#nav-clientes" role="tab" aria-controls="nav-clientes" aria-selected="false">clientes</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <!-- PARCEIROS -->
                    <div class="tab-pane fade show active" id="nav-parceiros" role="tabpanel" aria-labelledby="nav-parceiros-tab">
                        <div class="itens">

                            <!-- LOOP -->
                            <?php if(have_rows('marcas-parceiras')): while(have_rows('marcas-parceiras')) : the_row(); ?>
                                <div class="item">
                                    <a href="<?php the_sub_field('link-marca'); ?>" target="_blank"><img src="<?php the_sub_field('imagem-marca'); ?>"></a>
                                </div>
                            <?php endwhile; else : endif; ?>
                            <!-- ... -->
                            
                        </div>
                    </div>

                    <!-- CLIENTES -->
                    <div class="tab-pane fade" id="nav-clientes" role="tabpanel" aria-labelledby="nav-clientes-tab">
                        <div class="itens">
                            <!-- LOOP -->
                            <?php if(have_rows('marcas-clientes')): while(have_rows('marcas-clientes')) : the_row(); ?>
                                <div class="item">
                                    <a href="<?php the_sub_field('link-marca'); ?>" target="_blank"><img src="<?php the_sub_field('imagem-marca'); ?>"></a>
                                </div>
                            <?php endwhile; else : endif; ?>
                            <!-- ... -->
                        </div>
                    </div>
                </div>


            </div>
        </section>


        <!-- CHAMA O RODAPE -->
        <?php require 'footer.php' ?>

    </div>
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>