<?php
    // Template Name: Agenciados
?>

    <!-- CHAMA O HEADER WP -->
    <?php get_header(); ?>
    
    <div class="page-agenciados">

        <!-- CHAMA O CABECALHO - HEADER -->
        <?php require 'templates/cabecalho.php' ?>


        <!-- LISTA AGENCIADOS -->
        <div class="lista-agenciados">
            <div class="container">
                <div class="agenciados">
                    <nav>
                        <div class="nav nav-tabs" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-atores-tab" data-toggle="tab" href="#nav-atores" role="tab" aria-controls="nav-atores" aria-selected="true">Atores</a>
                            <a class="nav-item nav-link" id="nav-atrizes-tab" data-toggle="tab" href="#nav-atrizes" role="tab" aria-controls="nav-atrizes" aria-selected="false">Atrizes</a>
                            <a class="nav-item nav-link" id="nav-apresentadores-tab" data-toggle="tab" href="#nav-apresentadores" role="tab" aria-controls="nav-apresentadores" aria-selected="false">Apresentadores</a>
                            <a class="nav-item nav-link" id="nav-atletas-tab" data-toggle="tab" href="#nav-atletas" role="tab" aria-controls="nav-atletas" aria-selected="false">Atletas</a>
                            <a class="nav-item nav-link" id="nav-palestrantes-tab" data-toggle="tab" href="#nav-palestrantes" role="tab" aria-controls="nav-palestrantes" aria-selected="false">Palestrantes</a>
                            <a class="nav-item nav-link" id="nav-chefs-tab" data-toggle="tab" href="#nav-chefs" role="tab" aria-controls="nav-chefs" aria-selected="false">Chef's</a>
                        </div>
                    </nav>
                    <div class="tab-content" id="nav-tabContent">
                        <!-- ATORES -->
                        <div class="tab-pane fade show active" id="nav-atores" role="tabpanel" aria-labelledby="nav-atores-tab">
                            <div class="itens">
                                <!-- ... -->
                                <?php
                                    $args = array (
                                        'post_type' => 'artistas_agenciados', //Pega os post types no array para ser mostrado nos post
                                        'tipo_artista' => 'ator',
                                        'orderby' => 'title', 'order' => 'ASC',
                                        'posts_per_page'=> -1
                                    );
                                    $the_query = new WP_Query ( $args );
                                ?>
                                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="item">
                                    <a href="<?php the_permalink();?>">
                                        <div class="imagem-item">
                                            <?php the_post_thumbnail()?>
                                        </div>
                                        <div class="info-item">
                                            <div class="nome"><?php the_title()?></div>
                                            <!-- <div class="funcao"><?php the_field('funcao'); ?></div> -->
                                        </div>
                                    </a>
                                </div>

                                <?php endwhile; else: endif; ?>
                                <!-- ... -->
                            </div>
                        </div>

                        <!-- ATRIZES -->
                        <div class="tab-pane fade" id="nav-atrizes" role="tabpanel" aria-labelledby="nav-atrizes-tab">
                            <div class="itens">
                                <!-- ... -->
                                <?php
                                    $args = array (
                                        'post_type' => 'artistas_agenciados', //Pega os post types no array para ser mostrado nos post
                                        'tipo_artista' => 'atriz',
                                        'orderby' => 'title', 'order' => 'ASC',
                                        'posts_per_page'=> -1
                                    );
                                    $the_query = new WP_Query ( $args );
                                ?>
                                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="item">
                                    <a href="<?php the_permalink();?>">
                                        <div class="imagem-item">
                                            <?php the_post_thumbnail()?>
                                        </div>
                                        <div class="info-item">
                                            <div class="nome"><?php the_title()?></div>
                                            <!-- <div class="funcao"><?php the_field('funcao'); ?></div> -->
                                        </div>
                                    </a>
                                </div>

                                <?php endwhile; else: endif; ?>
                                <!-- ... -->
                            </div>
                        </div>

                        <!-- APRESENTADORES -->
                        <div class="tab-pane fade" id="nav-apresentadores" role="tabpanel" aria-labelledby="nav-apresentadores-tab">
                            <div class="itens">
                                <!-- ... -->
                                <?php
                                    $args = array (
                                        'post_type' => 'artistas_agenciados', //Pega os post types no array para ser mostrado nos post
                                        'tipo_artista' => 'apresentador',
                                        'orderby' => 'title', 'order' => 'ASC',
                                        'posts_per_page'=> -1
                                    );
                                    $the_query = new WP_Query ( $args );
                                ?>
                                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="item">
                                    <a href="<?php the_permalink();?>">
                                        <div class="imagem-item">
                                            <?php the_post_thumbnail()?>
                                        </div>
                                        <div class="info-item">
                                            <div class="nome"><?php the_title()?></div>
                                            <!-- <div class="funcao"><?php the_field('funcao'); ?></div> -->
                                        </div>
                                    </a>
                                </div>

                                <?php endwhile; else: endif; ?>
                                <!-- ... -->
                            </div>
                        </div>

                        <!-- ATLETAS -->
                        <div class="tab-pane fade" id="nav-atletas" role="tabpanel" aria-labelledby="nav-atletas-tab">
                            <div class="itens">
                                <!-- ... -->
                                <?php
                                    $args = array (
                                        'post_type' => 'artistas_agenciados', //Pega os post types no array para ser mostrado nos post
                                        'tipo_artista' => 'atleta',
                                        'orderby' => 'title', 'order' => 'ASC',
                                        'posts_per_page'=> -1
                                    );
                                    $the_query = new WP_Query ( $args );
                                ?>
                                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="item">
                                    <a href="<?php the_permalink();?>">
                                        <div class="imagem-item">
                                            <?php the_post_thumbnail()?>
                                        </div>
                                        <div class="info-item">
                                            <div class="nome"><?php the_title()?></div>
                                            <!-- <div class="funcao"><?php the_field('funcao'); ?></div> -->
                                        </div>
                                    </a>
                                </div>

                                <?php endwhile; else: endif; ?>
                                <!-- ... -->
                            </div>
                        </div>

                        <!-- PALESTRANTES -->
                        <div class="tab-pane fade" id="nav-palestrantes" role="tabpanel" aria-labelledby="nav-palestrantes-tab">
                            <div class="itens">
                                <!-- ... -->
                                <?php
                                    $args = array (
                                        'post_type' => 'artistas_agenciados', //Pega os post types no array para ser mostrado nos post
                                        'tipo_artista' => 'palestrante',
                                        'orderby' => 'title', 'order' => 'ASC',
                                        'posts_per_page'=> -1
                                    );
                                    $the_query = new WP_Query ( $args );
                                ?>
                                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="item">
                                    <a href="<?php the_permalink();?>">
                                        <div class="imagem-item">
                                            <?php the_post_thumbnail()?>
                                        </div>
                                        <div class="info-item">
                                            <div class="nome"><?php the_title()?></div>
                                            <!-- <div class="funcao"><?php the_field('funcao'); ?></div> -->
                                        </div>
                                    </a>
                                </div>

                                <?php endwhile; else: endif; ?>
                                <!-- ... -->
                            </div>
                        </div>

                        <!-- CHEFS -->
                        <div class="tab-pane fade" id="nav-chefs" role="tabpanel" aria-labelledby="nav-chefs-tab">
                            <div class="itens">
                                <!-- ... -->
                                <?php
                                    $args = array (
                                        'post_type' => 'chefs_agenciados', //Pega os post types no array para ser mostrado nos post
                                        'orderby' => 'rand',
                                        'posts_per_page'=> -1
                                    );
                                    $the_query = new WP_Query ( $args );
                                ?>
                                <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

                                <div class="item">
                                    <a href="<?php the_permalink();?>">
                                        <div class="imagem-item">
                                            <?php the_post_thumbnail()?>
                                        </div>
                                        <div class="info-item">
                                            <div class="nome"><?php the_title()?></div>
                                            <!-- <div class="funcao"><?php the_field('funcao'); ?></div> -->
                                        </div>
                                    </a>
                                </div>

                                <?php endwhile; else: endif; ?>
                                <!-- ... -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- CHAMA O RODAPE -->
        <?php require 'footer.php' ?>

    </div>
    <!-- WP -->
    <?php wp_footer(); ?>
</body>
</html>